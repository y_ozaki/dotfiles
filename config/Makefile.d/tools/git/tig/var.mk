TIG_DIR = $(VAR_DIR)/plugins/git/tig
TIG_REPO = git://github.com/jonas/tig.git

DISTCLEAN_TARGETS += tig-distclean
INSTALL_TARGETS += tig-install
UPDATE_TARGETS += tig-update
